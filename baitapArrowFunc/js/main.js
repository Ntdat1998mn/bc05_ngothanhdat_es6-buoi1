/* Kiểm tra xem trong class list đã có active chưa */
var oMauDuocChon = document.querySelectorAll("#colorContainer .color-button")
let kiemTraClassList = (viTri) => {
    let danhSachClassList = oMauDuocChon[viTri].classList
    for ( let i = 0; i < danhSachClassList.length; i++) {
        if ( danhSachClassList[i] == "active" ) {
            return true
        }
    }
}

/* Thêm active và đổi màu biến  --primary-color */
for (let index = 0; index < oMauDuocChon.length; index ++) {
    chonMau = oMauDuocChon[index].addEventListener('click', () => {
        if (kiemTraClassList(index) !== true) {
            oMauDuocChon.forEach((curValue)=> {
                curValue.classList.remove("active")
            })
            oMauDuocChon[index].classList.add("active")
        }
        const layClassDoiMauPri = oMauDuocChon[index].classList[1]
        
        const troToiClass = document.querySelector(`.${layClassDoiMauPri}`)
        
        const layPrimaryColor =  getComputedStyle(troToiClass).getPropertyValue('--primary-color')
         document.querySelector(':root').style.setProperty('--primary-color', `${layPrimaryColor}`) 

     })
}


